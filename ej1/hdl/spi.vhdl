------------------------------------------------------------------------------
-- Company: 
--
-- File: spi.vhdl
-- Description: 
--
-- 
--
-- Author: Bradley Shea
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity spi is
generic (
    C_AXI_DATA_WIDTH : integer :=32;
    C_AXI_ADDR_WIDTH : integer :=4;
    CLK_DIV          : integer :=50;
    SPI_REG_WIDTH    : integer :=8
);
port(
    SCLK        : out std_logic;
    CSn         : out std_logic;
    MOSI        : out std_logic;
    MISO        : in std_logic;
    INT         : out std_logic;
		
    WDATA       : in std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
    WADDR       : in std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
    WENA        : in std_logic;
	
    RDATA       : out std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
    RADDR       : in std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
    RENA        : in std_logic;
		
    AXI_CLK     : in std_logic;
    AXI_RESETn  : in std_logic
);
end entity;

architecture rtl of spi is

    type SPI_STATE is (
        IDLE,
        SHIFT,
        DONE
    );
    signal NEXT_SPI_STATE   : SPI_STATE;
	
    type AXI_STATE is (
        IDLE,
        WAIT_CS0,
        WAIT_CS1
    );
    signal NEXT_AXI_STATE   : AXI_STATE;

    signal rx_axi_i         : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
    signal rx_spi_i         : std_logic_vector(SPI_REG_WIDTH-1 downto 0);
    signal tx_axi_i         : std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
    signal tx_spi_i         : std_logic_vector(SPI_REG_WIDTH-1 downto 0);
    signal sclk_i           : std_logic;
    signal busy_i           : std_logic;
    signal start_i          : std_logic;
    signal cs_i             : std_logic;
    signal int_i            : std_logic;
	
    signal count_i          : integer range 0 to 100 := 1;
    signal count_spi_clk_i  : integer range 0 to 100 := 0;

begin
	
    --assign registers to outputs
    SCLK  <= sclk_i when cs_i = '0' else '0';
    CSn   <= cs_i;
    INT   <= int_i;
    MOSI  <= tx_spi_i(SPI_REG_WIDTH-1);
    RDATA <= rx_axi_i;
	
    --SCLK Gen
    process(AXI_CLK, AXI_RESETn)
    begin
        if (AXI_RESETn = '0') then
            sclk_i  <= '0';
            count_i <= 1;
			
        elsif (rising_edge(AXI_CLK)) then
            if (count_i < CLK_DIV) then
                count_i <= count_i + 1;
			
            else
                sclk_i	<= not sclk_i;
                count_i	<= 1;
				
            end if;
        end if;
    end process;
	
	
    --AXI/SPI Handshake State Machine
    process(AXI_CLK, AXI_RESETn)
    begin
        if (AXI_RESETn = '0') then
            tx_axi_i       <= (others => '0');
            rx_axi_i       <= (others => '0');
            start_i        <= '0';
            NEXT_AXI_STATE <= IDLE;
			
        elsif (rising_edge(AXI_CLK)) then
            start_i <= '0';
			
            --put last data received on read bus
            if (RENA = '1' and RADDR = x"4") then
                rx_axi_i <= tx_axi_i;
            end if;
			
            case NEXT_AXI_STATE is
                when IDLE =>
                    if (WENA = '1' and WADDR = x"0") then
                        start_i        <= '1';
                        tx_axi_i       <= WDATA;
                        NEXT_AXI_STATE <= WAIT_CS0;
                    end if;
				
                when WAIT_CS0 =>
                    start_i <= '1';
					
                    if (cs_i = '0') then
                        NEXT_AXI_STATE <= WAIT_CS1;
                    end if;
					
                when WAIT_CS1 =>
                    if (cs_i = '1') then
                        NEXT_AXI_STATE <= IDLE;
                    end if;
				
                when OTHERS => 
                    NEXT_AXI_STATE <= IDLE;
            end case;
		
        end if;
    end process; 
	
	
    --SPI State Machine
    process(sclk_i, AXI_RESETn)
    begin
        if (AXI_RESETn = '0') then
            NEXT_SPI_STATE  <= IDLE;
            rx_spi_i        <= (others => '0');
            tx_spi_i        <= (others => '0');
            int_i           <= '0';
            cs_i            <= '1';
            count_spi_clk_i <= 0;
			
        elsif (falling_edge(sclk_i)) then
            int_i           <= '0';
            cs_i            <= '1';
            count_spi_clk_i <= count_spi_clk_i;
			
            case NEXT_SPI_STATE is
                when IDLE =>
                    if (start_i = '1') then
                        cs_i           <= '0';
                        tx_spi_i       <= tx_axi_i(SPI_REG_WIDTH-1 downto 0);
                        NEXT_SPI_STATE <= SHIFT;
                    end if;
					
                when SHIFT =>
                    cs_i            <= '0';
                    count_spi_clk_i <= count_spi_clk_i + 1;
				
                    --TX
                    tx_spi_i        <= tx_spi_i(SPI_REG_WIDTH-2 downto 0) & '0';
					
                    --RX
                    rx_spi_i        <= rx_spi_i(SPI_REG_WIDTH-2 downto 0) & MISO;
					
                    --check if done with shift
                    if (count_spi_clk_i = SPI_REG_WIDTH-1) then
                        count_spi_clk_i	<= 0;
                        cs_i            <= '1';
                        NEXT_SPI_STATE	<= DONE;
                    end if;
					
                when DONE =>
                    int_i          <= '1';
                    NEXT_SPI_STATE <= IDLE;
				
                when OTHERS =>
                    NEXT_SPI_STATE <= IDLE;	
			
            end case;
        end if;
		
    end process;
	
end rtl;

	
